import { writeFileSync } from "fs";
import path from "path";
import { Column, DataType, Model, Sequelize, Table } from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { EMAIL_NAME, EMAIL_DOMAIN, EMAIL_LOCALE } = process.env;

const task1 = () => {
  if (typeof EMAIL_NAME !== "string" || EMAIL_NAME.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "EMAIL_NAME не был отправлен" }));
    return;
  }

  if (typeof EMAIL_DOMAIN !== "string" || EMAIL_DOMAIN.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "EMAIL_DOMAIN не был отправлен" }));
    return;
  }

  if (typeof EMAIL_LOCALE !== "string" || EMAIL_LOCALE.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "EMAIL_LOCALE не был отправлен" }));
    return;
  }

  const result = `${EMAIL_NAME}@${EMAIL_DOMAIN}.${EMAIL_LOCALE}`;

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        EMAIL_NAME,
        EMAIL_DOMAIN,
        EMAIL_LOCALE,
      },
      result,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  //Ищем всех студентов у кого есть 2 в оценках
  const studentList = await Student.findAll({
    order: [["birthdayDate", "ASC"]],
  });

  writeFileSync(filePathForTask2, JSON.stringify(studentList));
};

initDB();
